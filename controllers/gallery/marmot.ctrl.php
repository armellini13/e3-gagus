<?php

class GalleryMarmotController extends Controller
{
    public function build( )
    {
        $info = $this->getParams();

        if(!$info['error']) {
            $marmotNumber = $info["number"];

            if($marmotNumber == "")
                $marmotNumber = 0;

            // Agafem la galeria
            $photoModel = $this->getClass('GalleryGalleryModel');
            $gallerySize = $photoModel->getSize('marmot');
            $gallery = $photoModel->getMeAnimal('marmot');

            // Depenent dels casos mostrarem la galeria d'una manera o un altre
            // Si no esta buida

            if(isset($gallery[0]) && $marmotNumber < $gallerySize){

                $this->assign('finalMarmot',false);
                $this->assign('marmotName',$gallery[$marmotNumber]['name']);
                // Si estem al principi
                if(!$marmotNumber){

                    $this->assign('marmotURL_0',$gallery[$gallerySize - 1]['URL']);
                    $this->assign('marmotURL_1',$gallery[$marmotNumber]['URL']);

                    // Hi ha més d'una foto
                    if($gallerySize > 1)
                        $this->assign('marmotURL_2',$gallery[$marmotNumber + 1]['URL']);

                    // Nomes hi ha una foto
                    else
                        $this->assign('marmotURL_2','');
                }

                else {
                    // Estem al mitj
                    if($marmotNumber < $gallerySize - 1){
                        // Normal
                        $this->assign('marmotURL_0',$gallery[$marmotNumber - 1]['URL']);
                        $this->assign('marmotURL_1',$gallery[$marmotNumber]['URL']);
                        $this->assign('marmotURL_2',$gallery[$marmotNumber + 1]['URL']);
                    }
                    // Quasi al final
                    else {
                        $this->assign('marmotURL_0',$gallery[$marmotNumber - 1]['URL']);
                        $this->assign('marmotURL_1',$gallery[$marmotNumber]['URL']);
                        $this->assign('marmotURL_2','');
                    }
                }
            }

            else {
                $this->assign('finalMarmot',true);
                $this->assign('marmotName','');
            }

            $this->setLayout( 'gallery/marmot.tpl' );
        }
    }
}


?>