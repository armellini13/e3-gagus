<?php

class GalleryPlatypusController extends Controller
{
    public function build( )
    {
        $info = $this->getParams();

        if(!$info['error']) {

            $platypusNumber = $info["number"];

            if($platypusNumber == "")
                $platypusNumber = 0;

            // Agafem la galeria
            $photoModel = $this->getClass('GalleryGalleryModel');
            $gallerySize = $photoModel->getSize('platypus');
            $gallery = $photoModel->getMeAnimal('platypus');

            // Depenent dels casos mostrarem la galeria d'una manera o un altre
            // Si no esta buida
            if(isset($gallery[0]) && $platypusNumber < $gallerySize){

                $this->assign('finalPlatypus',false);
                $this->assign('platypusName',$gallery[$platypusNumber]['name']);

                // Si estem al principi
                if(!$platypusNumber){

                    $this->assign('platypusURL_0',$gallery[$gallerySize - 1]['URL']);
                    $this->assign('platypusURL_1',$gallery[$platypusNumber]['URL']);
                    // Hi ha més d'una foto
                    if($gallerySize > 1)
                        $this->assign('platypusURL_2',$gallery[$platypusNumber + 1]['URL']);

                    // Nomes hi ha una foto
                    else
                        $this->assign('platypusURL_2','');
                }

                else {
                    // Estem al mitj
                    if($platypusNumber < $gallerySize - 1){
                        // Normal
                        $this->assign('platypusURL_0',$gallery[$platypusNumber - 1]['URL']);
                        $this->assign('platypusURL_1',$gallery[$platypusNumber]['URL']);
                        $this->assign('platypusURL_2',$gallery[$platypusNumber + 1]['URL']);
                    }
                    // Quasi al final
                    else {
                        $this->assign('platypusURL_0',$gallery[$platypusNumber - 1]['URL']);
                        $this->assign('platypusURL_1',$gallery[$platypusNumber]['URL']);
                        $this->assign('platypusURL_2','');
                    }
                }
            }

            else {
                $this->assign('finalPlatypus',true);
                $this->assign('platypusName','');
            }

            $this->setLayout( 'gallery/platypus.tpl' );
        }
    }
}


?>