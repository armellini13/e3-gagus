<?php

class GalleryMonkeyController extends Controller
{
    public function build( )
    {
        $info = $this->getParams();

        if(!$info['error']) {

            $monkeyNumber = $info["number"];

            if($monkeyNumber == "")
                $monkeyNumber = 0;

            // Agafem la galeria
            $photoModel = $this->getClass('GalleryGalleryModel');
            $gallerySize = $photoModel->getSize('monkey');
            $gallery = $photoModel->getMeAnimal('monkey');

            // Depenent dels casos mostrarem la galeria d'una manera o un altre
            // Si no esta buida
            if(isset($gallery[0]) && $monkeyNumber < $gallerySize){

                $this->assign('finalMonkey',false);
                $this->assign('monkeyName',$gallery[$monkeyNumber]['name']);
                // Si estem al principi
                if(!$monkeyNumber){

                    $this->assign('monkeyURL_0',$gallery[$gallerySize - 1]['URL']);
                    $this->assign('monkeyURL_1',$gallery[$monkeyNumber]['URL']);

                    // Hi ha més d'una foto
                    if($gallerySize > 1)
                        $this->assign('monkeyURL_2',$gallery[$monkeyNumber + 1]['URL']);

                    // Nomes hi ha una foto
                    else
                        $this->assign('monkeyURL_2','');
                }

                else {
                    // Estem al mitj
                    if($monkeyNumber < $gallerySize - 1){
                        // Normal
                        $this->assign('monkeyURL_0',$gallery[$monkeyNumber - 1]['URL']);
                        $this->assign('monkeyURL_1',$gallery[$monkeyNumber]['URL']);
                        $this->assign('monkeyURL_2',$gallery[$monkeyNumber + 1]['URL']);
                    }
                    // Quasi al final
                    else {
                        $this->assign('monkeyURL_0',$gallery[$monkeyNumber - 1]['URL']);
                        $this->assign('monkeyURL_1',$gallery[$monkeyNumber]['URL']);
                        $this->assign('monkeyURL_2','');
                    }
                }
            }

            else {
                $this->assign('finalMonkey',true);
                $this->assign('monkeyName','');
            }

            $this->setLayout( 'gallery/monkey.tpl' );
        }
    }
}


?>