<?php

/*
 * Monkey Controller:
 * S'encarrega de mostrar la pàgina amb el mico corresponent.
 */
class ParentUploadController extends Controller
{
    protected $view = 'parent/upload.tpl';

    /**
     * Aquest m�tode sempre s'executa i caldr� implementar-lo sempre.
     */
    public function build()
    {
        $error = false;

        // Agafem els paràmetres
        $info = $this->getParams();

        if(isset($info["url_arguments"]) &&
            !(sizeof($info["url_arguments"]) == 1 && $info["url_arguments"][0] == "")) {

            $error = true;

        }

        // Si hi ha error
        if($error)
            $this->setLayout('error/error404.tpl');

        else {

            $this->setLayout($this->view);

        }

    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        $modules['marmot']  = 'UploadMarmotController';
        $modules['monkey']  = 'UploadMonkeyController';
        $modules['platypus'] = 'UploadPlatypusController';
        return $modules;
    }
}