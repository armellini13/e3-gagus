<?php

/*
 * Monkey Controller:
 * S'encarrega de mostrar la pàgina amb el mico corresponent.
 */
class ParentGalleryController extends Controller
{
    protected $view = 'parent/gallery.tpl';

    /**
     * Aquest m�tode sempre s'executa i caldr� implementar-lo sempre.
     */
    public function build()
    {

        // Flags de control
        $error = false;
        $noNumber = false;

        // Agafem els paràmetres
        $info = $this->getParams();
        $number = 0;

        // Agafem la mida de la galeria
        $galleryModel = $this->getClass('GalleryGalleryModel');
        $gallerySize = $galleryModel->getSizeGallery();

        // Comprovem errors a la URL
        if(isset($info["url_arguments"])) {

            $number = $info["url_arguments"][0];

            if(sizeof($info["url_arguments"]) == 1) {
                if($info["url_arguments"][0] == "")
                    $noNumber = true;
                else {
                    if(!($number >= 0 && $number <= $gallerySize))
                        $error = true;
                    else
                        if($number == 0)
                            $noNumber = true;
                }
            }
            else {
                if(!(sizeof($info["url_arguments"]) == 2 && $info["url_arguments"][1] == ""))
                    $error = true;
            }
        }
        else
            $noNumber = true;

        // Mirem si hi ha error
        if($error) {
            $this->setParams(array('error'=> true));
            $this->setLayout('error/error404.tpl');
        }

        else {
            // Assignem el numero de l'animal
            $final = false;
            $this->setParams(array('error'=> false));

            if($noNumber)
                $this->setParams(array('number'=> 0));

            else {
                $this->setParams(array('number'=> $number));
                if($gallerySize  == $number) {
                    $final = true;
                }
            }

            $this->assign('final',$final);
            $this->assign('numero',$number);
            $this->assign('seguent',$number + 1);
            $this->assign('anterior',$number - 1);

            $this->setLayout($this->view);
        }
    }

    /**
     * With this method you can load other modules that we will need in our page. You will have these modules availables in your template inside the "modules" array (example: {$modules.head}).
     * The sintax is the following:
     * $modules['name_in_the_modules_array_of_Smarty_template'] = Controller_name_to_load;
     *
     * @return array
     */
    public function loadModules() {
        $modules['head']	= 'SharedHeadController';
        $modules['footer']	= 'SharedFooterController';
        $modules['marmot']  = 'GalleryMarmotController';
        $modules['monkey']  = 'GalleryMonkeyController';
        $modules['platypus'] = 'GalleryPlatypusController';
        return $modules;
    }
}