<?php

/*
 * Model que s'encarrega de guardar les fotos dels micos
 * que es pujan a la pàgina web.
 */
class GalleryGalleryModel extends Model
{

    // Genera un ID per la foto
    private function getMeAnimalID($table,$url) {

        $sql = <<<QUERY
SELECT
    count(*)
FROM
    $table;
QUERY;

        return $this->getAll($sql)[0]['count(*)'].substr($url,5);

    }


    // Guarda el l'animal dins de la DB
    public function setAnimal($table, $nom, $url) {

        $id = $this->getMeAnimalID($table,$url);

        $sql = <<<QUERY
INSERT INTO
    $table
VALUES ( ?, ? , ? );
QUERY;


        $this->execute($sql,array( $id, $nom, $url));
    }

    // Genera un ID per la foto
    public function getMeAnimal($table) {

        $sql = <<<QUERY
SELECT
    name, URL
FROM
    $table;
QUERY;

        return $this->getAll($sql);

    }

    public function getSize($table) {

        $sql = <<<QUERY
SELECT
    count(*)
FROM
    $table;
QUERY;

        return $this->getAll($sql)[0]['count(*)'];
    }

    public function getSizeGallery(){

        $platypus = $this->getSize('platypus');
        $monkey = $this->getSize('monkey');
        $marmot = $this->getSize('marmot');

        if($marmot >= $platypus && $marmot >= $monkey)
            return $marmot;
        else {
            if($platypus >= $monkey)
                return $platypus;
            else
                return $monkey;
        }
    }

}